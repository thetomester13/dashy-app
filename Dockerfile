FROM cloudron/base:3.2.0@sha256:ba1d566164a67c266782545ea9809dc611c4152e27686fd14060332dd88263ea

ARG VERSION=2.0.3

RUN mkdir -p /app/code/dashy /app/data && chown -R cloudron:cloudron /app/code

RUN curl -L https://github.com/Lissy93/dashy/archive/${VERSION}.tar.gz | tar -xz --strip-components 1 -f - -C /app/code/dashy

WORKDIR /app/code/dashy

RUN yarn install
RUN yarn build

# Switch working directory so we can move /dashy and symlink it
WORKDIR /app/code

# Move /dashy to /app/data so we can write to it, and symlink it to /app/code
RUN cp -r /app/code/dashy /app/data/dashy
RUN rm -rf /app/code/dashy
RUN ln -s /app/data/dashy /app/code

COPY start.sh /app/code/

# Switch working directory back to our /dashy symlink
WORKDIR /app/code/dashy

CMD [ "/app/code/start.sh" ]
