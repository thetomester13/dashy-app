#!/bin/bash
set -eu

chown -R cloudron:cloudron /app/data

if [[ ! -f /app/data/.initialized ]]; then
  echo "=> Setup config"
  sed -i.bak "s+^\(PORT=\).*+\13000+" /app/data/dashy/.env
  sed -i.bak "s+^\(HOST=\).*+\1${CLOUDRON_APP_DOMAIN}+" /app/data/dashy/.env
  sed -i.bak "s+^\(VUE_APP_DOMAIN=\).*+\1${CLOUDRON_APP_DOMAIN}+" /app/data/dashy/.env

  touch /app/data/.initialized
fi

export PORT=3000

echo "=> Starting Dashy"

exec /usr/local/bin/gosu cloudron:cloudron npm run start