# Dashy Cloudron App

This repository contains the Cloudron app package source for [Dashy](https://github.com/Lissy93/dashy).

## Building

The app package can be built using the [Cloudron command line tooling](https://cloudron.io/references/cli.html).

```
cd dashy-app

cloudron build
cloudron install
```

## Notes

- Dashy needs to access to edit some files on the filesystem to update the actual dashboards. It frequently updates /public/conf.yml (as well as creating backups of conf.yml files whenever a change is made), and it rewrites the entire /dist directory after a configuration change is made. This resulted in me having to put the entire /dashy directory in /app/data. Not sure if this is the right way to go about it, but it works. 
- I noticed that when using the Update Configuration => Edit Config option, one must first Apply Locally or Preview Changes before writing the changes to the config file works properly. I'm going to assume that this is the behavior of the app itself, and not a result of my packaging :)
- When Dashy does rebuild its files after a configuration change, it ends up taking up a lot of memory. I've set mine to 1.5GB and haven't had issues. If you're seeing issues when trying to rebuild after a config change, try giving the app more resources. 
